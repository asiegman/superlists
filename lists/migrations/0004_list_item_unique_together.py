# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        (b'lists', b'0003_item_list'),
    ]

    operations = [
        migrations.AlterField(
            model_name=b'item',
            name=b'list',
            field=models.ForeignKey(to=b'lists.List', default=None, to_field='id'),
        ),
        migrations.AlterField(
            model_name=b'item',
            name=b'text',
            field=models.TextField(default=b''),
        ),
        migrations.AlterUniqueTogether(
            name=b'item',
            unique_together=set([(b'list', b'text')]),
        ),
    ]
