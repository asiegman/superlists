Working through the following book:

http://chimera.labs.oreilly.com/books/1234000000754/

Setup Notes:
1. Create Virtual Environment
2. Activate Virtual Environment
3. pip install https://www.djangoproject.com/download/1.7b1/tarball/
4. pip install -r requirements.txt
5. python manage.py migrate
6. Run it!
